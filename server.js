const express = require('express');
const hbs = require('hbs');
const fs = require('fs');

var app = express();

hbs.registerPartials(__dirname + '/views/partials');
hbs.registerHelper('getCurrentYear', () => {
  return new Date().getFullYear();
});
hbs.registerHelper('screamIt', (text) => {
  return text.toUpperCase();
});
app.set('view engine', 'hbs');

app.use((req, res, next) => {
  var now = new Date().toString();
  var log = `${now} : ${req.method} ${req.url}`;

  console.log(log);
  fs.appendFile('server.log', log + "\n", (err) => {
    if(err) {
      console.log('unable to append to server.log');
    }
  });
  next();
});

// app.use((req, res, next) => {
//   res.render('maintenance.hbs');
//   next();
// });
app.use(express.static(__dirname + '/public'));



app.get('/' , (request, response) => {
  //res.send('<h1>Hello Express!!!</h1>');
  response.send({
    name: "Ivan Draga",
    likes: ['Boxing', 'Dread Locks']
  });
});

app.get('/about', (request,response) =>{
  response.render('about.hbs', {
    pageTitle : 'AboutPage',
  });
});

app.get('/home', (request, response) => {
  response.render('home.hbs', {
    pageTitle : 'home',
    pageName : 'homePage'
  });
});

app.get('/bad', (request, response) =>{
  response.send({
    exception: "invalid address",
    errMsg: "unable to fetch address"
  });
});

app.listen(3000, () =>{
  console.log('server is up on port 300000');
});
